require 'sinatra'
require 'sinatra/cross_origin'
require 'json'

set server: 'thin', connections: []

configure do
    enable :cross_origin
end

before do
    response.headers['Access-Control-Allow-Origin'] = '*'
end

get '/' do
    File.read(File.join('public', 'index.html'))
end

options '*' do
    response.headers['Allow'] = 'GET, POST, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token'      
    response.headers['Access-Control-Allow-Origin'] = '*'
end

get '/stream', provides: 'text/event-stream' do
    stream(:keep_open) do |out|
        settings.connections << out
        out.callback { settings.connections.delete(out) }
    end
end

post '/frequency' do
    data = request.body.read.gsub("\n", "")
    settings.connections.each { |out| out << "event: message\ndata: #{data}\n\n" }
    204
end
