Din
======

Welcome to the wonderful world of sound visualization. Din is an experiment that shows how easy it is to manipulate a sound wave to get at its sub-frequencies.

# Usage
Din exists as a tiny sinatra server and a whole lotta javascript. The frontend will display a visualization for a frequency provided by a request to the sinatra server.

Use postman or curl to make a `POST` request to 

`https://dinviz.herokuapp.com/frequency`

Add your name, frequency, and CSS-supported color to the body:

```json
{
  "name": "Lord Farquad",
  "frequency": 1875,
  "color": "#d80258"
}
```

`Min Frequency: 0 Hz, Max Frequency 4500 Hz`

[Color Picker](https://www.google.com/search?ei=-PfkW7j2NsKUzwKRrom4Aw&q=hex+color+picker&oq=hex+color+picker&gs_l=psy-ab.3..35i39j0l9.2335.3564..3709...0.0..0.125.749.7j1......0....1..gws-wiz.......0i71.gwQZHnk4-x8)

Play around with the color and frequency and watch the visualization change!

# How Is This Possible?
Thank Joseph Fourier 

![Joey F.](https://upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Fourier2.jpg/250px-Fourier2.jpg)

for realizing that a complex wave is simply the sum of many simpler waves.
With this knowledge, we can separate the simpler waves into discrete frequency "buckets".

Check out my favorite website for more info: [Better Explained: Fourier
Transform](https://betterexplained.com/articles/an-interactive-guide-to-the-fourier-transform/)

