
* Intro
    * Digital signal processing is the crux of doing anything meaningful with real-world information
    * Last job: algorithms team digital signal processing
    * Tracker example - given a signal from the radar, keep history and predict heading and velocity of target
    * Made possible by decomposing a signal into constituent parts
* FFT - Fast Fourier Transform
    * Joseph Fourier made an amazing discovery - a complex signal can be described by the sum of simpler signals
    * E.g. Given a smoothie, pour it into a filter to get out the ingredients (strawberry, banana, mango)
    * Who cares? E.g. if we want to see if this smoothie is identical to another - easier to work with quantity of ingredients than to compare smoothies as whole entities
* Din
    * [pull up blank din instance]
    * First foray into DSP, made simpler by javascript audio browser API
    * Goal is to take a complex digital signal (mic input), and analyze its simpler signals (frequency values)
    * Idea is ripples on a pond - colors are individual frequencies, width is frequency strength
    * Actually frequencies are divided into buckets.
    * [pull up din repo readme] - to use, choose a frequency and make a postman POST request to din
    * Can also bring up din on your machines and receive everyone's requests.
* Architecture/Flow walk-through
    * requests -> backend
    * SSEs -> frontend
    * data translation -> visualization
* Code walk-through
    * SSE registration
        * add ripple function
        * add owner to set
    * Get User Media
        * Key to getting frequency data
    * AnalyzeStream()
        * adds ripples to array
    * two.bind('update')
        * visual linchpin
* Gratitude
* Questions
